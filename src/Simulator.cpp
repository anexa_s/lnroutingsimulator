#include "Simulator.h"
#include <queue>
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>
#include <random>

using namespace std;

Simulator::Simulator()
{
    graph = reader.ReadAndBuildGraph();
    reader.BuildLocalGraph(graph, NUMBER_OF_HOPS);
    transactions = reader.ReadTransactions();
    BuildCoordinateSystem(NUMBER_OF_LANDMARKS);
}

vector<int> Simulator::ElectLandmarks(int num_of_landmarks){
    // TODO
    vector<int> landmarks(1,0);
    return landmarks;
}

void Simulator::BuildCoordinateSystem(int num_of_landmarks)
{
    // Get Landmarks
    vector<int> landmarks = ElectLandmarks(num_of_landmarks);

    for (int i = 0; i < num_of_landmarks; i++)
    {
        int landmark = landmarks[i];
        graph.nodes[landmark].coordinates.push_back("0");
        
        // Setting up BFS
        queue<int> bfs;
        vector<int> visited(graph.node_count,0);
        vector<int> counter(graph.node_count,1);
        visited[landmark] = 1;
        bfs.push(landmark);

        while(!bfs.empty())
        {
            int parent = bfs.front();
            string parent_coordinate = graph.nodes[parent].coordinates[i];
            bfs.pop();
            vector<int> connections = graph.nodes[parent].connections;
            for(int edge : connections)
            {
                int child = graph.getOtherNode(edge, parent);
                if(visited[child] > 0)
                    continue;
                if(visited[parent] == 1){
                    // First time only choose with some percentage
                    if (rand() % 100 < COORDINATE_ACCEPTANCE_PERCENTAGE)
                    {
                        graph.nodes[child].coordinates.push_back(parent_coordinate + "-" + to_string(counter[parent]));
                        counter[parent]++;
                        visited[child] = 1;
                        bfs.push(child);
                    }
                }
                else 
                {
                    // Second time select compulsorily 
                    graph.nodes[child].coordinates.push_back(parent_coordinate + "-" + to_string(counter[parent]));
                    counter[parent]++;
                    visited[child] = 1;
                    bfs.push(child);
                }
            }

            if(visited[parent] == 1)
            {
                visited[parent] = 2;
                bfs.push(parent);
            }
        }
    }
}


long long Simulator::CheckTotalAmount(Graph& graph) {
    long long total = 0;
    for(auto edge: graph.edges) {
        total += edge.edge_data[ENODE1][EBALANCE];
        total += edge.edge_data[ENODE2][EBALANCE];
    }
    return total;
}

void Simulator::RunTransactions(int type, int t_id, bool verbose)
{
    long long inital_total_balance = CheckTotalAmount(graph);
    if(type == CLOUDY_ROUTING)
    {
        stringstream transactions_ss;

        CloudyRouting cloudy_routing(graph, K_NODES_TO_CHOOSE);
        int transactions_size = transactions.size();
        int last_time_stamp = transactions_size/10;
        int position = 0;
        ordered_set currently_running;
        for(int time = 0; time <= last_time_stamp; time++)
        {
            // create batch 
            currently_running.clear();
            while(position < transactions_size && transactions[position].start_time == time)
            {
                currently_running.insert(position);
                position++;
            }
            
            // run this batch until all complete
            while(currently_running.size())
            {   
                int rand_val = rand()%currently_running.size();
                auto transaction_id_itr = currently_running.find_by_order(rand_val);

                int status = cloudy_routing.run(transactions[*transaction_id_itr]);
                if(t_id == *transaction_id_itr) {
                    cloudy_routing.Log(transactions[*transaction_id_itr], status, transactions_ss);
                }

                if(verbose) {
                    cout << "Transaction id: " << *transaction_id_itr << ", " << "Status: " << status << '\n';
                }
                if(status != ALL_IS_WELL)
                {
                    cloudy_routing.Evaluate(transactions[*transaction_id_itr], status);
                    currently_running.erase(transaction_id_itr);
                }
            }
        }
        long long final_total_balance = CheckTotalAmount(graph);
        assert(inital_total_balance == final_total_balance);
        if(t_id != -1) {
            fstream f;
            f.open(LOG_DIR + "transaction_" + to_string(t_id) + ".txt", ios::out);
            if(f.is_open()) {
                f << transactions_ss.str();
                f.close();
            }
        }

        if(1) {
            cout << "Total Fee: " << cloudy_routing.total_fee << '\n';
            cout << "Total Transactions: " << cloudy_routing.transaction_count << '\n';
            cout << "Total Successful Transactions: " << cloudy_routing.successful_transaction_count << '\n';
        }
    }
    else if (type == SPEEDY_MURMURS)
    {
        stringstream transactions_ss;

        SpeedyMurmurs speedy_murmurs(graph);
        int transactions_size = transactions.size();
        int last_time_stamp = transactions_size/10;
        int position = 0;
        ordered_set currently_running;

        for(int time = 0; time <= last_time_stamp; time++)
        {
            // create batch 
            currently_running.clear();
            while(position < transactions_size && transactions[position].start_time == time)
            {
                currently_running.insert(position);
                position++;
            }
            
            // run this batch until all complete
            while(currently_running.size())
            {   
                int rand_val = rand()%currently_running.size();
                auto transaction_id_itr = currently_running.find_by_order(rand_val);

                int status = speedy_murmurs.run(transactions[*transaction_id_itr]);
                if(t_id == *transaction_id_itr) {
                    speedy_murmurs.Log(transactions[*transaction_id_itr], status, transactions_ss);
                }

                if(verbose) {
                    cout << "Transaction id: " << *transaction_id_itr << ", " << "Status: " << status << '\n';
                }
                if(status != ALL_IS_WELL)
                {
                    speedy_murmurs.Evaluate(transactions[*transaction_id_itr], status);
                    currently_running.erase(transaction_id_itr);
                }
            }
        }

        long long final_total_balance = CheckTotalAmount(graph);
        assert(inital_total_balance == final_total_balance);
        if(t_id != -1) {
            fstream f;
            f.open(LOG_DIR + "transaction_" + to_string(t_id) + ".txt", ios::out);
            if(f.is_open()) {
                f << transactions_ss.str();
                f.close();
            }
        }   

        // speedy_murmurs.graph.PrintGraph();

        if(1) {
            cout << "Total Fee: " << speedy_murmurs.total_fee << '\n';
            cout << "Total Transactions: " << speedy_murmurs.transaction_count << '\n';
            cout << "Total Successful Transactions: " << speedy_murmurs.successful_transaction_count << '\n';
        }
    }

}
