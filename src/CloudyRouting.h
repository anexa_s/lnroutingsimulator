#ifndef CloudyRouting_H
#define CloudyRouting_H

#include "Algorithm.h"

class CloudyRouting : public Algorithm
{
public:
    Graph *graph;

    // Choose till kth optimal node
    int k_nodes_considering;

    CloudyRouting();
    CloudyRouting(Graph& _graph, int _k_nodes_considering);

    // TODO:prove code for multiple landmarks
    // Finds the path till the next cloud node and returns kth path,fees 
    virtual pair<vector<int>,long long> FindNextNode(Transaction& transaction, int k, int choose_node);

    // Runs the algorithm one step at a time
    virtual int run(Transaction& transaction);

    // Logging current transaction information
    void Log(Transaction &transaction, int status, stringstream& ss);
};
#endif  