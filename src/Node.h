#ifndef Node_H
#define Node_H

#include "LocalGraph.h"

class Node{
private:
    /* data */
public:
    int node_id;
    LocalGraph localgraph;

    // contain edge ids
    vector<int> connections;

    // format ex:- 0-9-8-7-23-18
    vector<string> coordinates;

    Node();

    // Printing values in node
    void PrintNode();

};

#endif