#ifndef SpeedyMurmurs_H
#define SpeedyMurmurs_H

#include "Algorithm.h"

class SpeedyMurmurs : public Algorithm
{
public:
    Graph *graph;
    SpeedyMurmurs(Graph& _graph);

    // TODO:prove code for multiple landmarks
    // Finds the path till the next cloud node and returns kth path,fees 
    virtual pair<vector<int>,long long> FindNextNode(Transaction& transaction, int k, int choose_node);

    // Runs the algorithm one step at a time
    virtual int run(Transaction& transaction);    

    // Logging current transaction information
    void Log(Transaction &transaction, int status, stringstream& ss);

};

#endif