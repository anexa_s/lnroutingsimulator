#ifndef Simulator_H
#define Simulator_H

#include "Reader.h"
#include "Algorithm.h"
#include "CloudyRouting.h"
#include "SpeedyMurmurs.h"
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

const int COORDINATE_ACCEPTANCE_PERCENTAGE = 50;
const int NUMBER_OF_HOPS = 5;
const int NUMBER_OF_LANDMARKS = 1;
const int CLOUDY_ROUTING = 1;
const int SPEEDY_MURMURS = 2;
const int K_NODES_TO_CHOOSE = 15;

const string LOG_DIR = "logs/";

#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_update>
//member functions :
//1. order_of_key(k) : number of elements strictly lesser than k
//2. find_by_order(k) : k-th element in the set

using namespace __gnu_pbds;

class Simulator
{
public:
    vector<Transaction> transactions;
    Graph graph;
    Reader reader;
    

    // elects landmarks in the graph
    vector<int> ElectLandmarks(int num_of_landmarks);

    // build co-ordinate system for each landmark
    void BuildCoordinateSystem(int num_of_landmarks);

    // Simulates transactions
    void RunTransactions(int type, int t_id=-1, bool verbose=false);

    // Checks the total balance of graph
    long long CheckTotalAmount(Graph &graph);

    Simulator(/* args */);
};

#endif