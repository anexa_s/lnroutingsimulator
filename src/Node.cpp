#include "Node.h"
#include <iostream>

Node::Node()
{

}

void Node::PrintNode()
{
    cout<<"---------------------------------------------------\n";
    cout<<"Node Id : "<<node_id<<"\n";
    cout<<"Edges :";
    for(int edge : connections)
    {
        cout<<" "<<edge;
    }
    cout<<"\n";
    if(coordinates.size())
    {
        cout<<"Coordinates :";
        for(string coordinate :coordinates)
        {
            cout<<" "<<coordinate;
        }
        cout<<"\n";
    }
    localgraph.PrintLocalGraph();
    cout<<"---------------------------------------------------\n";
}