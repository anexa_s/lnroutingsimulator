#ifndef LocalGraph_H
#define LocalGraph_H

#include <map>
#include <vector> 
#include <iostream>
using namespace std;

class LocalGraph
{
private:
    /* data */
public:
    // Map from nodeId to neighbouring edges. Stored as vector<Edge Id>
    map<int,vector<int>> adj;

    // Mean of fee and variance of the nodes in local graph
    double fee_mean;
    double fee_variance;

    LocalGraph();

    // Printing Local graph values
    void PrintLocalGraph();
};

#endif