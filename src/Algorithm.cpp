#include "Algorithm.h"

Algorithm::~Algorithm(){

}

int Algorithm::ExecuteNextNode(Transaction& transaction, Graph& graph) {
    assert(transaction.transaction_state == transaction.Executing);

    // Get next edge details and next node
    int next_edge_id = transaction.edges_remaining.front();
    int next_node = graph.getOtherNode(next_edge_id, transaction.current_node);

    int current_node_idx = graph.getNodeIndex(next_edge_id, transaction.current_node);
    long long balance = graph.edges[next_edge_id].edge_data[current_node_idx][EBALANCE];
    long long base_fee = graph.edges[next_edge_id].edge_data[current_node_idx][EBASE_FEE];
    long long multiplier = graph.edges[next_edge_id].edge_data[current_node_idx][EMULTIPLIER];

    //TODO multiplier problems
    long long total_fee = base_fee + multiplier * transaction.amount;

    // TODO make fee of sender 0

    // Check for errors
    if(transaction.amount - transaction.fees_so_far.back() > balance) {
        transaction.transaction_state = transaction.Reverting;
        return INSUFFICIENT_BALANCE;
    }

    if(total_fee + transaction.fees_so_far.back() > transaction.fee_limit) {
        transaction.transaction_state = transaction.Reverting;
        return FEE_LIMIT_EXCEEDED;
    }

    // Update edge details
    transaction.fees_so_far.push_back(transaction.fees_so_far.back() + total_fee);
    graph.edges[next_edge_id].edge_data[current_node_idx][EBALANCE] -= (transaction.amount - transaction.fees_so_far.back());
    graph.edges[next_edge_id].edge_data[current_node_idx][ELOCKED_BALANCE] += (transaction.amount - transaction.fees_so_far.back());

    // Remove this 
    if(transaction.current_node == transaction.cloud_nodes[transaction.cloud_nodes_passed].first)
        transaction.cloud_nodes_passed++;

    // Update transaction
    transaction.edges_done.push_back(next_edge_id);
    transaction.edges_remaining.pop_front();
    transaction.current_node = next_node;
    return ALL_IS_WELL;
}

void Algorithm::FinalisePreviousNode(Transaction& transaction, Graph& graph) {
    assert(transaction.transaction_state == transaction.Finalizing);

    // Get previous edge details and nodes
    int previous_edge_id = transaction.edges_done.back();
    int previous_node = graph.getOtherNode(previous_edge_id, transaction.current_node);
    int previous_node_idx = graph.getNodeIndex(previous_edge_id, previous_node);
    int current_node_idx = graph.getNodeIndex(previous_edge_id, transaction.current_node);


    // Update edge details
    graph.edges[previous_edge_id].edge_data[current_node_idx][EBALANCE] += (transaction.amount - transaction.fees_so_far.back());
    graph.edges[previous_edge_id].edge_data[previous_node_idx][ELOCKED_BALANCE] -= (transaction.amount - transaction.fees_so_far.back());
    transaction.fees_so_far.pop_back();

    // Update transaction 
    transaction.edges_done.pop_back();
    transaction.current_node = previous_node;
}

void Algorithm::RevertToPreviousNode(Transaction& transaction, Graph& graph) {
    assert(transaction.transaction_state == transaction.Reverting);

    // Get previous edge details and previous node
    int previous_edge_id = transaction.edges_done.back();
    int previous_node = graph.getOtherNode(previous_edge_id, transaction.current_node);
    int previous_node_idx = graph.getNodeIndex(previous_edge_id, previous_node); 
    
    // Update edge details
    graph.edges[previous_edge_id].edge_data[previous_node_idx][EBALANCE] += (transaction.amount - transaction.fees_so_far.back());
    graph.edges[previous_edge_id].edge_data[previous_node_idx][ELOCKED_BALANCE] -= (transaction.amount - transaction.fees_so_far.back());
    transaction.fees_so_far.pop_back();

    // Update transaction
    transaction.edges_done.pop_back();
    transaction.current_node = previous_node;
}

void Algorithm::Evaluate(Transaction& transaction, int status) {

    if (status == ALL_IS_WELL_FOREVER)
    {
        long long total_transaction_fee = 0;
        for (auto fee : transaction.cloud_fee)
        {
            total_transaction_fee += fee;
        }
        total_fee += total_transaction_fee;
        successful_transaction_count++;
    }

    transaction_count++;
}