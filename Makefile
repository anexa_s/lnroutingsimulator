CC=g++
CFLAGS=-g -Wall
JSON=-ljsoncpp
SRC=src
OBJ=obj
BINDIR=bin
SRCS=$(wildcard $(SRC)/*.cpp)
OBJS=$(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SRCS))
BIN=$(BINDIR)/main

ALL:$(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@ $(JSON)

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ $(JSON)

clean:
	$(RM) -r $(BINDIR)/* $(OBJ)/*
