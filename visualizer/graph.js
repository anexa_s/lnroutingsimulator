class Graph {
    constructor(node_count, edge_count, nodes, edges) {
        this.node_count = node_count;
        this.edge_count = edge_count;
        this.nodes = nodes; 
        this.edges = edges;
    }



    draw() {
        background(0);
        // if(random(1) > 0.5) {
        //     let x = floor(random(0, this.n));
        //     let c = lerp(this.edges[x].balance_a, 0, 0.01);
        //     this.edges[x].balance_a = floor(c);
        // }
        // else {
        //     let x = floor(random(0, this.n));
        //     let c = lerp(this.edges[x].balance_b, 0, 0.01);
        //     this.edges[x].balance_b = floor(c);
        // }
        for(let edge of this.edges) {
            edge.draw();
        }
        for(let node of this.nodes) {
            node.draw();
        }
    }

    mousePressed(x, y) {
        for(let node of this.nodes) {
            node.mousePressed(x, y);
        }
    }

    mouseDragged(x, y) {
        for(let node of this.nodes) {
            node.mouseDragged(x, y);
        }

    }

}