const R = 20;
const W = 800;
const H = 800;
const EDGE_WIDTH = 8;
const MEAN = 5000;
const SD = 200;
const NODE_COUNT = 'node count';
const EDGE_COUNT = 'edge count';
const NODES = 'nodes';
const NODE_ID = 'Node Id';
const NEIGHBOR_COUNT = 'Neighbor Count';
const NEIGHBORS = 'Neighbors';
const EDGES = 'edges';
const EDGE_ID = 'Edge Id';
const BALANCE = 'Balance';
const BASE_FEE = 'Base Fee';
const MULTIPLIER = 'Multiplier';
const ENODE_ID = 0;
const EBALANCE = 1;
const EBASE_FEE = 2;
const EMULTIPLIER = 3;


