let g;
let reader;




function preload() {
  reader = new Reader();
}
function setup() {
  createCanvas(W, H);
  textAlign(CENTER, CENTER);
  g = reader.constructGraph();
  
  
}

function draw() {
  background(220);
  g.draw();
}

function mousePressed() {
  g.mousePressed(mouseX, mouseY);
}

function mouseDragged() {
  g.mouseDragged(mouseX, mouseY);
}