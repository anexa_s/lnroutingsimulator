import json
import random

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import argparse

# Graph JSON Keys
GRAPH_FILE = "data/graph.JSON"
NODE_COUNT = "node count"
EDGE_COUNT = "edge count"
NODES = "nodes"
EDGES = "edges"
BALANCE = "Balance"
NODE_ID = "Node Id"
EDGE_ID = "Edge Id"
NEIGHBOUR_COUNT = "Neighbor Count"
NEIGHBOURS = "Neighbors"
BASE_FEE = "Base Fee"
MULTIPLIER = "Multiplier"
AMOUNT = 100000
FEE = 10
NEIGHBOUR_PROBABILITY = 0.6

# Transaction JSON Keys
TRANSACTION_FILE = "data/transactions.JSON"
TRANSACTION_ID = "Transaction Id"
TRANSACTION_COUNT = "transactions count"
TRANSACTIONS = "transactions"
SENDER = "Sender"
RECEIVER = "Receiver"
T_AMOUNT = "Amount"
START_TIME = "Start Time"
FEE_LIMIT = "Fee Limit"

class Graph:
    # Creates graph using barabasi albert model
    def __init__(self, n, m):
        self.G = nx.barabasi_albert_graph(n, m)
        self.adj = self.create_adj()

    def create_adj(self):
        adj = {}
        for edge in self.G.edges.data():
            u = edge[0]
            v = edge[1]
            if adj.get(u) is None:
                adj[u] = []
            if adj.get(v) is None:
                adj[v] = []
            adj[v].append(u)
            adj[u].append(v)
        return adj

    def add_node_attr(self):
        for i in range(len(self.G.nodes)):
            self.G.nodes[i][NODE_ID] = i
            channels = []
            for j in self.adj[i]:
                if j > i:
                    channels.append(self.G[i][j][EDGE_ID])
                else:
                    channels.append(self.G[j][i][EDGE_ID])
            self.G.nodes[i][NEIGHBOURS] = channels
            self.G.nodes[i][NEIGHBOUR_COUNT] = len(channels)

    # Uses constant balance and fee for all edges. TODO: try different model later
    def add_edge_attr(self):
        i = 0
        for edge in self.G.edges.data():
            u = edge[0]
            v = edge[1]
            self.G[u][v][EDGE_ID] = i
            edge_u = {
                NODE_ID: u,
                BALANCE: AMOUNT,
                BASE_FEE: FEE,
                MULTIPLIER: 0
            }
            edge_v = {
                NODE_ID: v,
                BALANCE: AMOUNT,
                BASE_FEE: FEE,
                MULTIPLIER: 0
            }
            self.G[u][v][NODES] = [edge_u, edge_v]
            i += 1

    def generate_graph_json(self):
        data = {NODE_COUNT: len(self.G.nodes), EDGE_COUNT: len(self.G.edges.data())}
        nodes = []
        for node in self.G.nodes.data():
            nodes.append(node[1])
        data[NODES] = nodes
        edges = []
        for edge in self.G.edges.data():
            edges.append(edge[2])
        data[EDGES] = edges
        return data
    
    # Generate transaction with a random sender and receiver as neighbour with prob W and not a neighbour with prob 1 - W
    # Transactions have constant Amount and constant number of concurrent transactions at a time
    def generate_transactions(self,transaction_count):
        transactions = []
        N = len(self.G.nodes)
        for i in range(transaction_count):
            sender = random.randint(0,N-1)
            n = self.G.nodes[sender][NEIGHBOUR_COUNT]
            try:            
                neighbour_weight = ((N-1)*NEIGHBOUR_PROBABILITY - n)/(N-1-n)
            except:
                print("Error N-1-n = 0")
            receiver = random.randint(0,N-1)
            while(receiver == sender):
                receiver = random.randint(0,N-1)
            # else:
            #     position = random.randint(0,n-1)
            #     receiver = self.adj[sender][position]
            trasaction = {
                TRANSACTION_ID : i,
                SENDER : sender,
                RECEIVER : receiver,
                START_TIME : i//10,
                T_AMOUNT : AMOUNT//10,
                FEE_LIMIT : 10000000
            }
            transactions.append(trasaction)
        return transactions

    def generate_trasaction_json(self,transactions):
        data = {TRANSACTION_COUNT: len(transactions)}
        data[TRANSACTIONS] = transactions
        return data
        

def dump_to_json_file(file_name, json_data):
    f = open(file_name, "w")
    json.dump(json_data, f, indent = 1)
    f.close()


# commands of the form [--arg arg_value]
def commands(parser):
    parser.add_argument(
        "-n", "--nodeCount",
        help="number of nodes in the graph",
        nargs=1,
        type=int,
        action="store"
    )

    parser.add_argument(
        "-m", "--avgDegree",
        help="average degree of the graph",
        nargs=1,
        type=int,
        action="store"
    )

    parser.add_argument(
        "-tc", "--transactionCount",
        help="number of transactions",
        nargs=1,
        type=int,
        action="store"
    )

# main function
def main():
    # create parser and the commands for arguments
    parser = argparse.ArgumentParser(description='Graph generator')
    commands(parser)

    # parse the arguments
    args = parser.parse_args()
    if args.nodeCount:
        n = args.nodeCount[0]
    if args.avgDegree:
        m = args.avgDegree[0]
    if args.transactionCount:
        tc = args.transactionCount[0]
    

    # generate graph
    g = Graph(n, m)
    g.add_edge_attr()
    g.add_node_attr()

    # generate json data
    transactions_json = g.generate_trasaction_json(g.generate_transactions(tc))
    graph_json = g.generate_graph_json()

    # dump to json file
    dump_to_json_file(GRAPH_FILE, graph_json)
    dump_to_json_file(TRANSACTION_FILE, transactions_json)


if __name__ == '__main__':
    main()


